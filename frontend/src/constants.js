export const G_USER_ID ='name'
export const G_USER_EMAIL ='email'
export const G_AUTH_TOKEN ='token'
export const ITEMS_PER_PAGE = 1 // setting this only to one so you can easily test your pagination implementation

export const APP_LANGUAGE ='app-language'